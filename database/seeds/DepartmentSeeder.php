<?php

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
           
            [
            'name' => "HR",
            'created_at'=> now(),
            'updated_at'=> now(),
            ],
            [
            'name' => "Management",
            'created_at'=> now(),
            'updated_at'=> now(),
            ],
            ]);
    }
}
