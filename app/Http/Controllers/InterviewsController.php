<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interview;
use App\Candidate;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
class IntreviewController extends Controller
{




    public function myinterviews(Request $request){

        $user_profile = Auth::id();
        $user = User::findorfail($user_profile);
        $interviews = $user->interview;
        $users =  User:: all();
        $candidates = Candidate::all();
        if($interviews->count()!=0){
        return view('interviews.index',compact('interviews','users','candidates'));}
        else{echo "no interviews";}


}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = Interview::all();
        return view('interviews.index', compact('interviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('assign-user',Auth::user()) Or Gate::authorize('assign-interview',Auth::user());
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.create',compact('candidates','users'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interview();
        $this-> validate($request,[
            'date' => 'required',
            'summary' => 'required',
            'candidate_id' =>'required',
            'user_id' =>'required'
        ]);
          $interview->create($request ->all());
          return redirect('interviews')->with('message','interview has been Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
