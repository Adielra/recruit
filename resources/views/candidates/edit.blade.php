@extends('layouts.app')

@section('content')

    <body>
        <h2>Edit candidate</h2>    
        <form method = "post" action = "{{action('candidatesController@update', $candidate->id)}}">
        @method('PATCH') 
        @csrf
    <div class ="full-height">
        <div>
                <label for = "name">Candidate name</label>
                <input class="form-control" type = "text" name = "name" value = {{$candidate->name}}>
        </div>
        <div>
                <label for = "email">Candidate email</label>
                <input class="form-control" type = "text" name = "email" value = {{$candidate->email}}>
        </div>
        <br>
        <div>
                 <input type = "submit" class="btn btn-outline-secondary" name ="submit" value = "update candidate">
        </div>
        </br>
        @if($errors->any())
        <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
         <li>{{$error}}</il>
        @endforeach 
        </ul>
        </div>
        @endif
        </form>
    </div>
@endsection

    </body>
    
</html>
