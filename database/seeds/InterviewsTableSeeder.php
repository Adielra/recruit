<?php

use Illuminate\Database\Seeder;

class InterviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
           
            'name' => Str::random(10),
            'email' => Str::random(6).'@gmail.com',
            'interview brief' => Str::random(60),
            'created_at'=> now(),
            'updated_at'=> now(),
        ]);
    }
}
