<?php

use Illuminate\Database\Seeder;

class CandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('candidates')->insert([
           
            'name' => Str::random(10),
            'email' => Str::random(6).'@gmail.com',
            'created_at'=> now(),
            'updated_at'=> now(),
        ]);
    }
}
