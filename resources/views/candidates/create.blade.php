@extends('layouts.app')

@section('content')

    <body>
        <h1>Create candidate</h1>    
        <form method = "post" action = "{{action('candidatesController@store')}}">
        @csrf 
        
        <div>
                <label for = "name">Candidate name</label>
                <input type = "text" class="form-control" name = "name">
        </div>
      
        <br>
        <div>
                <label for = "email">Candidate email</label>
                <input type = "text" class="form-control" name = "email">
        </div>
        </br>
        <br>
    
        <div>
                 <input type = "submit"  class="btn btn-outline-secondary"  name ="submit" value = "create candidate">
        </div>
        </br>
        
        
        @if($errors->any())
        <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
         <li>{{$error}}</il>
        @endforeach 
        </ul>
      
        </div> 
         @endif
        </form>
      
    </body>
@endsection
