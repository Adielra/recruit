<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $fillable = [
        'date', 'summary','candidate_id','user_id'
    ];

    public function candidateInter(){
        return $this->belongsTo('App\Candidate','candidate_id');
    }

    public function userInter(){
        return $this->belongsTo('App\User','user_id');

    }
}