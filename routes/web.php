<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('candidates','candidatesController')->middleware('auth');
Route::get('candidates/changeuser/{cid}/{uid?}', 'candidatesController@changeUser')->name('candidate.changeuser');                
 
Route::get('candidates/changestatus/{cid}/{sid}', 'candidatesController@changeStatus')->name('candidates.changestatus')->middleware('auth');

Route::get('mycandidates', 'candidatesController@myCandidates')->name('candidates.mycandidates')->middleware('auth');

Route::get('candidates/delete/{id}','candidatesController@destroy')->name('candidates.delete');
Route::resource('interviews', 'IntreviewController')->middleware('auth');
Route::get('/myinterviews', 'IntreviewController@myinterviews')->name('interviews.myinterviews')->middleware('auth');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function (){
    return 'Hello Laravel';
});

Route::get('/student/{id}', function ($id = 'No Student Found'){
    return 'we got student with id '.$id;
});

Route::get('/car/{id?}', function ($id = null){
    if(isset($id)){
       //TODO: validate for integer
        return "we got car $id";
    } else {
        return 'We need the id to find your car';
    }
});


Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id'));
});


##Exercise 5

Route::get('users/{email}/{name?}', function($email,$name = 'OOPS! Name Missing') {
      return view('users', compact('name','email'));
        
});
    
#######


       
    

    







Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
